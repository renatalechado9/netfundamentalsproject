﻿using System;
using NominaApp.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NominaApp.Dao
{
    public interface IDaoEmpleado : Interface1<Empleado>
    {
        Empleado findById(int id);
        Empleado findByCedula(string cedula);
    }
}
