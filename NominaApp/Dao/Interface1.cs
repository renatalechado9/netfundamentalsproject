﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NominaApp.Dao
{
    public interface Interface1<T>
    {
        void Create(T t);
        bool Update(T t);
        bool Delete(T t);
        List<T> All();
    }
}
