﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NominaApp.Entities
{
    public class Empleado
    {
        //private int id;
        //private string nombres;

        //public string Nombres
        //{
        //    get { return nombres; }
        //    set { nombres = value ; }
        //}

        //public int Id { get => id; set => id = value; }
        public int Id { get; set; } //4
        public string Nombres { get; set; } //40 + 3
        public string Apellidos { get; set; }//40 +3
        public string Cedula { get; set; } //32 +3
        public string Direccion { get; set; } //400 + 3
        public string Telefono { get; set; } // 30 + 3
        public decimal Salario { get; set; } //8
        public DateTime FechaContratacion { get; set; } // 8
        public string Foto { get; set; }//400 + 3

        public override string ToString()
        {
            return "Id: "+this.Id + ",Nombres : "+ this.Nombres+ ", Apellidos: "+ this.Apellidos + ",Cedula: "+ this.Cedula + ", Direccion: "+this.Direccion+ ", Telefono :"+ this.Telefono;
        }
    }
}
