﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NominaApp.Entities
{
    public class Indez
    {
        public int Id { get; set; }
        public Type IndexType { get; set; }
        public object Value { get; set; }
    }
}
