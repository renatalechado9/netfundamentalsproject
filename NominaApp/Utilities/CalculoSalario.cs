﻿using System;

namespace NominaApp.Utilities
{
    public class CalculoSalario
    {
        private const float factorInss = 0.07f;
        public static float Inss(float sb)
        {
            return sb * factorInss;
        }

        public static float IR(float sb)
        {
            float ir = 0;
            if (sb <= 0)
            {
                return ir;
            }
            float salanual = (sb - Inss(sb)) * 12;
            
            if(salanual > 0.01f && salanual <= 100000)
            {
                return ir;
            }
            else if(salanual > 100001 && salanual <= 200000)
            {
                ir = ((salanual - 100000) * 0.15f)/12;
            }
            else if(salanual >200001 && salanual <= 350000)
            {
                ir = (((salanual - 200000) * 0.20f) + 15000) / 12;
            }
            else if(salanual > 350001 && salanual <= 500000)
            {
                ir = (((salanual - 350000) * 0.25f) + 45000) / 12;
            }
            else
            {
                ir = (((salanual - 500000) * 0.30f) + 82500) / 12;
            }
            return ir;
        }
        public static float Antiguedad (DateTime fc, float sb)
        {
            int year = DateTime.Now.Year - fc.Year;
            float factor =(float) year / 100;
            return sb * factor;
        }

    }
}
